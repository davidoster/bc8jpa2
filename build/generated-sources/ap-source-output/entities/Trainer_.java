package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Trainer.class)
public abstract class Trainer_ {

	public static volatile SingularAttribute<Trainer, String> firstName;
	public static volatile SingularAttribute<Trainer, String> lastName;
	public static volatile SingularAttribute<Trainer, String> subject;
	public static volatile SingularAttribute<Trainer, Long> id;

}

