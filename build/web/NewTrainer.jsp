<%-- 
    Document   : newstudent
    Created on : Jun 26, 2019, 11:58:06 AM
    Author     : George.Pasparakis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New Student</title>
    </head>
    <body>
                
        <h1><%= request.getAttribute("title")%></h1>
        <form method="POST" action="insert_trainer">
            Name: <input name="name" type="text" /><br />
            Surname: <input name="surname" type="text" /><br />
            Subject <input name="subject" type="text" /><br />
            <input type="Submit" value="Save Trainer" /><br />
        </form>
    </body>
</html>
