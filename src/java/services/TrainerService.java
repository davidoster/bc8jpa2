/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.TrainerDao;
import entities.Trainer;
import java.util.Collection;

/**
 *
 * @author Dimitris
 */
public class TrainerService {

    public String getAllTrainers() {
        TrainerDao td = new TrainerDao();
        StringBuilder str
                = new StringBuilder();
        str.append(htmlHead());
        for (Trainer t : td.getAllTrainers()) {
            str.append("<p>")
                    .append(t)
                    .append("<a href=\"delete_trainer/").append(t.getId()).append("\" > Delete</a>")
                    .append("<a href=\"update_trainer?id=").append(t.getId()).append("\" > Update</a>")
                    .append("</p>");
        }
        str.append(htmlFoot());
        return str.toString();

    }

    public boolean insertTrainer(Trainer t) {
        TrainerDao td = new TrainerDao();
        return td.insertTrainer(t);
    }

    public boolean updateTrainer(Trainer t) {
        TrainerDao td = new TrainerDao();
        return td.updateTrainer(t);
    }

    public boolean deleteTrainerById(Long id) {
        TrainerDao td = new TrainerDao();
        return td.deleteTrainerById(id);
    }

    public Trainer findTrainerById(Long id) {
        TrainerDao td = new TrainerDao();
        return td.findTrainerById(id);
    }

    private String htmlHead() {
        StringBuilder str
                = new StringBuilder();
        str.append("<!DOCTYPE html>")
                .append("<html>")
                .append("<head>")
                .append("<title>My First Servlet</title>")
                .append("</head>")
                .append("<body>");
        return str.toString();
    }

    private String htmlFoot() {
        StringBuilder str
                = new StringBuilder();
        str.append("</body>")
                .append("</html>");
        return str.toString();
    }
}
