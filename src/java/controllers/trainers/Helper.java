package controllers.trainers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.List;
import java.util.Scanner;

public class Helper {

    public static final DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("dd/MM/uuuu HH:mm:ss").withResolverStyle(ResolverStyle.STRICT);
    public static final DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/uuuu").withResolverStyle(ResolverStyle.STRICT);
    public static final DateTimeFormatter dbDateFormat = DateTimeFormatter.ofPattern("uuuu-MM-dd").withResolverStyle(ResolverStyle.STRICT);
    public static final DateTimeFormatter dbDateTimeFormat = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss").withResolverStyle(ResolverStyle.STRICT);
    
    public static int[] checkIds(String s, List<Integer> aIDs, List<Integer> bIDs){
        int[] ids = {-1,-1};
        
        int aID = 0, bID = 0;
                if (s.contains("-") && s.length() >= 3) {
                    String[] relArray = s.split("-");
                    if (Helper.checkInt(relArray[0]) && Helper.checkInt(relArray[1])) {
                        aID = Integer.parseInt(relArray[0]);
                        bID = Integer.parseInt(relArray[1]);
                        if (aIDs.contains(aID) && bIDs.contains(bID)) {
                            ids[0]= aID;
                            ids[1]= bID;
                        } else {
                            System.out.println("Non existed IDs, Please choose from list!");
                        }
                    } else {
                        System.out.println("Wrong format, Please give numbers!");
                    }
                } else {
                    System.out.println("Wrong format, Please use dash(-) to seperate IDs!");
                }
        
        return ids;
    }
    public static boolean checkMark(String input){
        int mark;
        try{
            mark = Integer.parseInt(input);
        }
        catch(NumberFormatException e){
            System.out.println("Please give a number");
            return false;
        }
        if(mark>100 || mark<0){
            System.out.println("Mark should be between 0 and 100!");
            return false;
        }
        return true;
    }
    
    public static boolean checkInt(String input){
        try{
            Integer.parseInt(input);
        }
        catch(NumberFormatException e){
            System.out.println("Please give an integer number");
            return false;
        }
        return true;
    }
    
    public static boolean checkDouble(String input){
        try{
            Double.parseDouble(input);
        }
        catch(NumberFormatException e){
            System.out.println("Please give a number");
            return false;
        }
        return true;
    }
    
    public static boolean checkAfterDate(String date){
        if (checkDate(date)){
            LocalDate dt = LocalDate.parse(date, dateFormat);
            if (dt.isAfter(LocalDate.now())){
                return true;
            }else{
                System.out.println("Date must be after today");
                return false;
            }
        }
        return false;        
    }
    
    public static boolean checkAfterDate(String date, LocalDate compareDate){
        if (checkDate(date)){
            LocalDate dt = LocalDate.parse(date, dateFormat);
            if (dt.isAfter(compareDate)){
                return true;
            }else{
                System.out.println("Date must be after course start date");
                return false;
            }
        }
        return false;        
    }
    
    public static boolean checkBeforeDate(String date){
        if (checkDate(date)){
            LocalDate dt = LocalDate.parse(date, dateFormat);
            if (dt.isBefore(LocalDate.now())){
                return true;
            }else{
                System.out.println("Date must be before today");
                return false;
            }
        }
        return false;        
    }
    
    public static boolean checkDate(String date) {
        try {
            LocalDate.parse(date, dateFormat);
        }
        catch(DateTimeParseException e){
            System.out.println("Wrong date format or date does not exist!");
            return false;
        }
        return true;

    }
    public static boolean checkAfterDateTime(String date){
        if (checkDateTime(date)){
            LocalDateTime dt = LocalDateTime.parse(date, dateTimeFormat);
            if (dt.isAfter(LocalDateTime.now())){
                return true;
            }else{
                System.out.println("Date must be after today");
                return false;
            }
        }
        return false;        
    }
    public static boolean checkDateTime(String dateTime) {
        try {
            LocalDateTime.parse(dateTime, dateTimeFormat);
        }
        catch(DateTimeParseException e){
            System.out.println("Wrong date format or date does not exist!");
            return false;
        }
        return true;

    }

    public static boolean checkWord(String word) {
        boolean allLetters = word.chars().allMatch(Character::isLetter);
        if (allLetters && word.length() > 2) {
            return true;
        }
        System.out.println("Please give one word");
        return false;
    }
    
    public static boolean checkPhrase(String phrase) {
        if (phrase.length() > 0) {
            return true;
        }
        System.out.println("Input should not be emty");
        return false;
    }
    
    public static int importDataChoice(Scanner input, int range) {
        int choice = -1;

        if (input.hasNextInt()) {
            choice = input.nextInt();
            input.nextLine();//we remove '\n' from input

            if (choice < 0 || choice > range) {
                System.out.println("*ERROR: Please give a valid option*");
                choice = -1;
            }

        } else {
            System.out.println("*ERROR: Please give a valid choice*");
            input.nextLine();//we "clear" invalid input so user give new
        }
        return choice;
    }
}
