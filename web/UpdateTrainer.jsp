<%-- 
    Document   : newstudent
    Created on : Jun 26, 2019, 11:58:06 AM
    Author     : George.Pasparakis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="entities.Trainer" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>New Student</title>
    </head>
    <%! Trainer t = new Trainer(); %>
    <%= t = ((Trainer) request.getAttribute("t")) %>
    <body>
                
        <h1><%= request.getAttribute("title")%></h1>
        <form method="POST" action="update_trainer">
            <input name="id" type="hidden" value="<%= t.getId() %>" /><br />
            Name: <input name="firstname" type="text" value="<%= t.getFirstName() %>" /><br />
            Surname: <input name="lastname" type="text" value="<%= t.getLastName() %>" /><br />
            Subject <input name="subject" type="text" value="<%= t.getSubject() %>" /><br />
            <input type="Submit" value="Save Trainer" /><br />
        </form>
    </body>
</html>
